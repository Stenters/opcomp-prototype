const express = require('express');
const bcrypt = require('bcryptjs');
const sqlite3 = require('sqlite3').verbose();


const nJwt = require('njwt');
const config = require('./config');

const db = new sqlite3.Database('database.db'); //this db is only stored in memory. Will be deleted once server stops running

const jwtAuth = require('./auth');


db.serialize(() => {
  db.run("CREATE TABLE if not exists users (id INTEGER PRIMARY KEY, name TEXT, email TEXT, password TEXT)"); //creates a table with those rows
});

const router = express.Router(); //uses an express server

router.post('/register', function(req, res) {
  var hashedPassword = bcrypt.hashSync(req.body.password, 8); //This encrypts the password passed in during registration using bcrypt

  db.run("INSERT INTO users (name, email, password) "
        + "VALUES (?, ?, ?)", req.body.name, req.body.email, hashedPassword,
  function (err) {
    if (err) return res.status(500).send("An error occurred during registration");

    res.status(200).send({ status: 'ok' });
  }); //inserts the name, email, and hashed password into the database. 
  //If something goes wrong during the insertion, an http error of 500 is sent back. Otherwise ok message 200 is sent back
});

router.post('/login', function(req, res) { //retrieves the user from the database while trying to login
    db.get("SELECT id, name, email, password FROM users " 
          + "WHERE email=?", req.body.email, function (err, user) {
      if (err) return res.status(500).send({status: 'Server error', err:err}); //A 500 error is sent back if there is something goes wrong
      if (!user) return res.status(404).send('User not found'); //404 error is sent back if a user of that email is not found
  
      if (!bcrypt.compareSync(req.body.password, user.password)) { //compares the database password to the password passed in. Async is preferred over comparesync because, comparesync is cpu intensive.
        return res.status(401).send({ auth: false, token: null });
      }
  
      var jwt = nJwt.create({ id: user.id }, config.secret); //java web token created and sent back to the user's computer if authenticated. Whenever the user goes to page that is
      //unique to it's user's page, the java web token gets sent to the server to authenticate if the user has access to that information.
      jwt.setExpiration(new Date().getTime() + (24*60*60*1000)); //sets when the jwt expires. It expires in 24 hours.
  
      res.status(200).send({ auth: true, token: jwt.compact() });
    });
  });

  router.get('/profile', jwtAuth, function(req, res, next) { //gets the users profile after jwtAuth confirms the user has the correct jwt
    db.get("SELECT id, name, email FROM users WHERE id=?", req.userId, function (err, user) {
      if (err) {
        return res.status(500).send("There was a problem finding the user.");
      }
      if (!user) {
        return res.status(404).send("No user found.");
      }
      res.status(200).send(user);
    });
  });
  
module.exports = router;
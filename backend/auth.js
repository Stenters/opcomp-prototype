const nJwt = require('njwt');
var config = require('./config');

function jwtAuth(req, res, next) {
  if (!req.token) {
    return res.status(403).send({ auth: false, message: 'No token provided' });
  }

  nJwt.verify(req.token, config.secret, function(err, decoded) {//verifies if the user has the correct java web token using the signing key we saved
    if (err) {
      return res.status(500).send({ auth: false, message: 'Could not authenticate token' });
    }
    console.log("auth.js req.token: " + req.token);
    console.log("auth.js config.secret:" + config.secret);
    req.userId = decoded.body.id;
    next();
  });
}

module.exports = jwtAuth;
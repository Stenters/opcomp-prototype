import { Component, OnInit } from '@angular/core';

class ScoreArchive {
  name: string;
  problemURL: string;
  solutionURL: string;
  scoreURL: string;
  dataURL: string;

  constructor(name: string, problemURL: string, solutionURL: string, scoreURL: string, dataURL: string) {
    this.name = name;
    this.problemURL = problemURL;
    this.solutionURL = solutionURL;
    this.scoreURL = scoreURL;
    this.dataURL = dataURL;
  }
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'] 
})
export class HomeComponent implements OnInit {
  archive: Array<ScoreArchive>;

  getPreviousScoresFromDB(): Array<ScoreArchive> {
    // Stub method to generate crap data
    var archive = new Array<ScoreArchive>();
    var sampleData = [2019, 2018, 2017, 2016, 2015, 2014, 2013, 2003];

    for (var i in sampleData) {
      archive.push(new ScoreArchive(''+sampleData[i],'','','',''));
    }
    
    return archive;
  }

  constructor() {
    this.archive = this.getPreviousScoresFromDB();
  }

  ngOnInit(): void {}

}

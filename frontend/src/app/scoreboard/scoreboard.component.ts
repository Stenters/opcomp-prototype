import { Component, OnInit } from '@angular/core';
import { Team } from './team';


@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.scss']
})
export class ScoreboardComponent implements OnInit {
  count: number;
  total: number;
  teams: Array<Team>;

  constructor() { 
    this.teams = new Array<Team>();
    this.count = 10;
  }

   ngOnInit(): void {
      this.addTeam(new Team("testA"));
      this.addTeam(new Team("testB"));
      this.addTeam(new Team("testC"));

      this.updateScore("testA", 2, -10);
      this.updateScore("testB", 5, 8);
      this.updateScore("testA", 1, 10);
      this.updateScore("testC", 7, -2);

      this.count = this.teams.length;
    }

  addTeam(t: Team): void { this.teams.push(t); }

  getTeam(teamName: string): Team { return this.teams.find(t => t.name === teamName); }

  updateScore(teamName: string, question: number, value: number): void { this.getTeam(teamName).setScore(question, value); }

  getTeamsByScore(): Array<Team> { return this.teams.sort((a,b) => b.total - a.total); }

}

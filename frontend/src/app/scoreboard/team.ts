export class Team {
    name: string;
    scores: Array<Number>;
    total: number;
  
    constructor(name:string) {
        this.name = name;
        this.scores = new Array<Number>();
        this.total = 0;

        for (var i = 0; i <= 9; ++i) {
            this.scores[i] = 0;
        }
    }
  
    setScore(question: number, value: number): void {
        if (question < 1 || question > 9) {
        console.log(`Illegal assignment to team ${name} on question ${question} (should be 1-9)`);
        }

        this.scores[question] = value;
        this.total += value;
    }
  }
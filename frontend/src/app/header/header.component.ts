import { Component, OnInit } from '@angular/core';
import { IBreadCrumb } from './breadcrumb.interface';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { ActivatedRoute, Router, NavigationEnd, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public breadcrumbs: IBreadCrumb[];
  num : number = 5;

  getBreadcrumbs(activatedRoute: ActivatedRoute): IBreadCrumb[] {
    var breadcrumbs = this.buildBreadcrumbs(activatedRoute.root);

    if (breadcrumbs.length > 1 && breadcrumbs[1].label === "Home") {
      breadcrumbs = breadcrumbs.slice(1);
    } else {
      breadcrumbs[0].label = "Home";
    }

    return breadcrumbs;
  }


  constructor(router: Router, activatedRoute: ActivatedRoute) {
    this.breadcrumbs = this.getBreadcrumbs(activatedRoute)

    router.events.pipe(filter(e => e instanceof NavigationEnd)).subscribe(() => {
      this.breadcrumbs = this.getBreadcrumbs(activatedRoute)
  })
  }

  buildBreadcrumbs(route: ActivatedRoute, url: string = '', breadcrumbs: IBreadCrumb[] = []): IBreadCrumb[] {
    let label = route.routeConfig && route.routeConfig.data? route.routeConfig.data.breadcrumb : "";
    
    let path = route.routeConfig && route.routeConfig.data ? route.routeConfig.path : "";

    let lastRoute = path.split('/').pop();

    // check if dynamically generated route
    if (lastRoute.startsWith(':') && !!route.snapshot) {
      let paramName = lastRoute.slice(1);
      label = route.snapshot.params[paramName];
      path = path.replace(lastRoute, label);
    }

    let nextUrl = path ? `${url}/${path}` : url

    const breadcrumb: IBreadCrumb = { label: label, url: nextUrl };

    const newBreadcrumbs = [...breadcrumbs, breadcrumb];

    if (route.firstChild) {
      return this.buildBreadcrumbs(route.firstChild, nextUrl, newBreadcrumbs);
    }
    return newBreadcrumbs;
   
  }

  ngOnInit(): void {}

}

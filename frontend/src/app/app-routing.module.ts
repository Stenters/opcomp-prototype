import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component"
import {ProfileComponent} from "./profile/profile.component"
import {InformationComponent} from "./information/information.component"
import { Information_Rule_Component } from './information/information-rule.component';
import { Information_Instruction_Component } from './information/information-instruction.component';
import { Information_Practice_Component } from './information/information-practice.component';
import { Information_Data_Component } from './information/information-data.component';

const routes: Routes = [
  {
    path: '',
    data: {breadcrumb: 'Home'},
    component: HomeComponent,
  },
  {
    path:'login',
    component: LoginComponent,
    data: {breadcrumb: null},

  },
  { 
    path: 'register', 
    component: RegisterComponent, 
    data: {breadcrumb: 'register'} 
  
  },
  { 
    path: 'profile', 
    component: ProfileComponent, 
    data: {breadcrumb: 'profile'} 
  
  },
  {

    path: 'info',
    data: {breadcrumb: 'Information'},
    children: [
      {
        path: '',
        data: {breadcrumb: null},        
        component: InformationComponent,
      },
      {
        path: 'rules',
        data: {breadcrumb: 'Rules'},
        component: Information_Rule_Component,
      },
      {
        path: 'instructions',
        data: {breadcrumb: 'Instructions'},
        component: Information_Instruction_Component,
      },
      {
        path: 'practice',
        data: {breadcrumb: 'Practice'},
        component: Information_Practice_Component,
      },
      {
        path: 'data',
        data: {breadcrumb: 'Data'},
        component: Information_Data_Component,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

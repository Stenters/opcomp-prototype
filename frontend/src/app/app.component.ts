import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})

export class AppComponent {
  title = 'jwt-client';

  constructor(public authService: AuthService) {}

  onLogout() {
    this.authService.logout();
  }
}
